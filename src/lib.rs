#![feature(const_generics)]

#[macro_export]
macro_rules! quantity {
    ( $struct_name:ident [$array_name:ident]
        ( $($unit:ident $unit1:ident $zero:expr),* ) 
    ) => {
        // TODO: I think the issue here is that cfg_attr is being expanded in the crate depending on this,
        // figure out how to optionally disable serde support. Maybe have to have two whole quantity! defns.
        // #[cfg_attr(feature = "serde_support", derive(serde::Serialize, serde::Deserialize))]
        // #[cfg_attr(feature = "serde_support", serde(transparent))]
        #[derive(serde::Serialize, serde::Deserialize)]
        #[serde(transparent)]
        #[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
        pub struct $struct_name < $(const $unit: i32),* > (pub f64);

        // #[derive(serde::Serialize, serde::Deserialize)]
        // #[serde(transparent)]
        #[derive(Clone, Copy)]
        pub struct $array_name < const N: usize, $(const $unit: i32),* >(pub [$struct_name<$($unit),*>; N]);

        impl  <const N: usize, $(const $unit: i32),*> PartialEq for $array_name<N, $($unit),*> {
            fn eq(&self, rhs: &Self) -> bool {
                for i in 0.. N {
                    if self[i] != rhs[i] {
                        return false;
                    }
                }

                true
            }
        }

        impl  <const N: usize, $(const $unit: i32),*> std::fmt::Debug for $array_name<N, $($unit),*> {
            fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
                let mut list = fmt.debug_list();
                for i in 0.. N {
                    list.entry(&self[i]);
                }
                list.finish()
            }
        }

        impl <$(const $unit: i32),*> std::ops::Neg for $struct_name<$($unit),*> {
            type Output = Self;
            fn neg(self) -> Self {
                $struct_name(-self.0)
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::Neg for $array_name<N, $($unit),*> {
            type Output = Self;
            fn neg(mut self) -> Self {
                for i in 0.. N {
                    self[i] = -self[i]
                }
                self
            }
        }

        impl <$(const $unit: i32),*> std::ops::Add for $struct_name<$($unit),*> {
            type Output = Self;
            fn add(self, rhs: Self) -> Self {
                $struct_name(self.0 + rhs.0)
            }
        }


        impl <const N: usize, $(const $unit: i32),*> std::ops::Add for $array_name<N, $($unit),*> {
            type Output = Self;
            fn add(self, rhs: Self) -> Self {
                let mut arr = [Q(0.0); N];
                for i in 0.. N {
                    arr[i] = self.0[i] + rhs.0[i];
                }
                $array_name(arr)
            }
        }

        impl <$(const $unit: i32),*> std::ops::AddAssign for $struct_name<$($unit),*> {
            fn add_assign(&mut self, rhs: Self) {
                self.0 += rhs.0
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::AddAssign for $array_name<N, $($unit),*> {
            fn add_assign(&mut self, rhs: Self) {
                for i in 0.. N {
                    self.0[i] += rhs.0[i];
                }
            }
        }

        impl <$(const $unit: i32),*> std::ops::Sub for $struct_name<$($unit),*> {
            type Output = Self;
            fn sub(self, rhs: Self) -> Self {
                $struct_name(self.0 - rhs.0)
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::Sub for $array_name<N, $($unit),*> {
            type Output = Self;
            fn sub(self, rhs: Self) -> Self {
                let mut arr = [Q(0.0); N];
                for i in 0.. N {
                    arr[i] = self.0[i] - rhs.0[i];
                }
                $array_name(arr)
            }
        }

        impl <$(const $unit: i32),*> std::ops::SubAssign for $struct_name<$($unit),*> {
            fn sub_assign(&mut self, rhs: Self) {
                self.0 -= rhs.0
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::SubAssign for $array_name<N, $($unit),*> {
            fn sub_assign(&mut self, rhs: Self) {
                for i in 0.. N {
                    self.0[i] -= rhs.0[i];
                }
            }
        }

        impl <$(const $unit: i32, const $unit1: i32),*> std::ops::Mul<$struct_name<$($unit1),*>> for $struct_name<$($unit),*>  {
            type Output = $struct_name<$({$unit + $unit1}),*>;
            fn mul(self, rhs: $struct_name<$($unit1),*>) -> Self::Output {
                $struct_name(self.0 * rhs.0)
            }
        }

        impl <const N: usize, $(const $unit: i32, const $unit1: i32),*> std::ops::Mul<$struct_name<$($unit1),*>> for $array_name<N, $($unit),*>  {
            type Output = $array_name<N, $({$unit + $unit1}),*>;
            fn mul(self, rhs: $struct_name<$($unit1),*>) -> Self::Output {
                let mut arr = [Q(0.0); N];
                for i in 0.. N {
                    arr[i] = (self[i] * rhs).fix()
                }
                $array_name(arr)
            }
        }

        impl <const N: usize, $(const $unit: i32, const $unit1: i32),*> std::ops::Mul<$array_name<N, $($unit1),*>> for $array_name<N, $($unit),*>  {
            type Output = $array_name<N, $({$unit + $unit1}),*>;
            fn mul(self, rhs: $array_name<N, $($unit1),*>) -> Self::Output {
                let mut arr = [Q(0.0); N];
                for i in 0.. N {
                    arr[i] = (self[i] * rhs[i]).fix()
                }
                $array_name(arr)
            }
        }


        impl <$(const $unit: i32),*> std::ops::Mul<f64> for $struct_name<$($unit),*>  {
            type Output = $struct_name<$($unit),*>;
            fn mul(self, rhs: f64) -> Self::Output {
                $struct_name(self.0 * rhs)
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::Mul<f64> for $array_name<N, $($unit),*>  {
            type Output = $array_name<N, $($unit),*>;
            fn mul(self, rhs: f64) -> Self::Output {
                let mut arr = [Q(0.0); N];
                for i in 0.. N {
                    arr[i] = self.0[i] * rhs
                }
                $array_name(arr)
            }
        }

        impl <$(const $unit: i32),*> std::ops::MulAssign<$struct_name<$($zero),*>> for $struct_name<$($unit),*> {
            fn mul_assign(&mut self, rhs: $struct_name<$($zero),*>) {
                self.0 *= rhs.0
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::MulAssign<$struct_name<$($zero),*>> for $array_name<N, $($unit),*> {
            fn mul_assign(&mut self, rhs: $struct_name<$($zero),*>) {
                for i in 0.. N {
                    self.0[i] *= rhs
                }
            }
        }

        impl <$(const $unit: i32),*> std::ops::MulAssign<f64> for $struct_name<$($unit),*> {
            fn mul_assign(&mut self, rhs: f64) {
                self.0 *= rhs
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::MulAssign<f64> for $array_name<N, $($unit),*> {
            fn mul_assign(&mut self, rhs: f64) {
                for i in 0.. N {
                    self.0[i] *= rhs
                }
            }
        }

        impl <$(const $unit: i32, const $unit1: i32),*> std::ops::Div<$struct_name<$($unit1),*>> for $struct_name<$($unit),*>  {
            type Output = $struct_name<$({$unit - $unit1}),*>;
            fn div(self, rhs: $struct_name<$($unit1),*>) -> Self::Output {
                $struct_name(self.0 / rhs.0)
            }
        }

        impl <const N: usize, $(const $unit: i32, const $unit1: i32),*> std::ops::Div<$struct_name<$($unit1),*>> for $array_name<N, $($unit),*>  {
            type Output = $array_name<N, $({$unit - $unit1}),*>;
            fn div(self, rhs: $struct_name<$($unit1),*>) -> Self::Output {
                let mut arr = [Q(0.0); N];
                for i in 0.. N {
                    arr[i] = (self[i] / rhs).fix()
                }
                $array_name(arr)
            }
        }

        impl <const N: usize, $(const $unit: i32, const $unit1: i32),*> std::ops::Div<$array_name<N, $($unit1),*>> for $array_name<N, $($unit),*>  {
            type Output = $array_name<N, $({$unit - $unit1}),*>;
            fn div(self, rhs: $array_name<N, $($unit1),*>) -> Self::Output {
                let mut arr = [Q(0.0); N];
                for i in 0.. N {
                    arr[i] = (self[i] / rhs[i]).fix()
                }
                $array_name(arr)
            }
        }

        impl <$(const $unit: i32),*> std::ops::Div<f64> for $struct_name<$($unit),*>  {
            type Output = $struct_name<$($unit),*>;
            fn div(self, rhs: f64) -> Self::Output {
                $struct_name(self.0 / rhs)
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::Div<f64> for $array_name<N, $($unit),*>  {
            type Output = $array_name<N, $($unit),*>;
            fn div(self, rhs: f64) -> Self::Output {
                let mut arr = [Q(0.0); N];
                for i in 0.. N {
                    arr[i] = self.0[i] / rhs
                }
                $array_name(arr)
            }
        }

        impl <$(const $unit: i32),*> std::ops::DivAssign<$struct_name<$($zero),*>> for $struct_name<$($unit),*> {
            fn div_assign(&mut self, rhs: $struct_name<$($zero),*>) {
                self.0 /= rhs.0
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::DivAssign<$struct_name<$($zero),*>> for $array_name<N, $($unit),*> {
            fn div_assign(&mut self, rhs: $struct_name<$($zero),*>) {
                for i in 0.. N {
                    self.0[i] /= rhs
                }
            }
        }

        impl <$(const $unit: i32),*> std::ops::DivAssign<f64> for $struct_name<$($unit),*> {
            fn div_assign(&mut self, rhs: f64) {
                self.0 /= rhs
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::DivAssign<f64> for $array_name<N, $($unit),*> {
            fn div_assign(&mut self, rhs: f64) {
                for i in 0.. N {
                    self.0[i] /= rhs
                }
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::Index<usize> for $array_name<N, $($unit),*> {
            type Output = $struct_name<$($unit),*>;
            fn index(&self, i: usize) -> &Self::Output {
                &self.0[i]
            }
        }

        impl <const N: usize, $(const $unit: i32),*> std::ops::IndexMut<usize> for $array_name<N, $($unit),*> {
            fn index_mut(&mut self, i: usize) -> &mut Self::Output {
                &mut self.0[i]
            }
        }

        impl <$(const $unit: i32),*> $struct_name<$($unit),*> {
            /// "Temporary" hack that run-time checks constants are equal, for use when compile time
            /// checking fails
            ///
            /// Many uses of this will probably be fixed by https://github.com/rust-lang/rust/pull/67890
            pub fn fix <$(const $unit1: i32),*> (self) -> $struct_name<$($unit1),*> {
                $(debug_assert_eq!($unit, $unit1);)*
                $struct_name(self.0)
            }

            // Not a method because rustc won't accept the necessary type signature in an impl block
            pub fn sqrt(v: $struct_name<$({$unit * 2}),*>) -> $struct_name<$($unit),*> {
                $struct_name(v.0.sqrt())
            }

            pub fn abs(self) -> Self {
                $struct_name(self.0.abs())
            }

            pub fn clamp(self, min: Self, max: Self) -> Self {
                debug_assert!(min <= max);
                if self < min {
                    min
                }
                else if self > max {
                    max
                }
                else {
                    self
                }
            }
        }

        impl <const N: usize, $(const $unit: i32),*> $array_name<N, $($unit),*> {
            pub fn abs(self) -> Self {
                let mut arr = [$struct_name(0.0); N];
                for i in 0.. N {
                    arr[i] = self[i].abs();
                }
                $array_name(arr)
            }

            pub fn sum(self) -> $struct_name<$($unit),*> {
                let mut out = Q(0.0);
                for i in 0.. N {
                    out += self[i];
                }
                out
            }

            pub fn norm(self) -> $struct_name<$($unit),*> {
                $struct_name::<$($unit),*>::sqrt((self * self).sum().fix())
            }

            pub fn max(self) -> $struct_name<$($unit),*> {
                let mut max = self[0];
                for i in 1.. N {
                    if self[i] > max {
                        max = self[i];
                    }
                }
                max
            }

            pub fn min(self) -> $struct_name<$($unit),*> {
                let mut min = self[0];
                for i in 1.. N {
                    if self[i] < min {
                        min = self[i];
                    }
                }
                min
            }

            pub fn clamp(mut self, min: Self, max: Self) -> Self {
                for i in 0.. N {
                    self[i] = self[i].clamp(min[i], max[i]);
                }
                
                self
            }
        }

        impl <$(const $unit: i32),*> $array_name<2, $($unit),*> {
            pub fn new(x: $struct_name<$($unit),*>, y: $struct_name<$($unit),*>) -> Self {
                $array_name([x, y])
            }
        }

    }
}

struct TestQ<const X: i32, const Y: i32>(f64);

impl <const X: i32, const Y: i32> std::ops::Add for TestQ<X, Y> {
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        TestQ(self.0 + rhs.0)
    }
}

impl <const X: i32, const X1: i32, const Y: i32, const Y1: i32> std::ops::Mul<TestQ<X1, Y1>> for TestQ<X, Y> {
    type Output = TestQ<{X + X1}, {Y + Y1}>;
    fn mul(self, rhs: TestQ<X1, Y1>) -> Self::Output { 
        TestQ(self.0 * rhs.0)
    }
}
